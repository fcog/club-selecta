//build.gradle

apply plugin: 'com.android.application'

android {
    compileSdkVersion 20
    buildToolsVersion "20.0.0"

    defaultConfig {
        applicationId "com.elpais.app.clubselecta"
        minSdkVersion 9
        targetSdkVersion 20
        versionCode 1
        versionName "1.0"
    }
    buildTypes {
        release {
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
//    compile('com.mapbox.mapboxsdk:mapbox-android-sdk:0.4.0@aar') {
//        transitive = true
//    }
//    compile('com.cocoahero.android:geojson:1.0.0@aar') {
//        transitive = true
//    }
    compile 'com.actionbarsherlock:actionbarsherlock:4.4.0@aar'
    compile 'com.android.support:support-v4:21.0.0'
    compile 'com.mcxiaoke.volley:library:1.0.6'
    compile 'com.google.android.gms:play-services:6.1.11'
//    compile 'com.android.support:appcompat-v7:21.0.0'
}
