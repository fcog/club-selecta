package com.elpais.app.clubselecta.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.elpais.app.clubselecta.fragments.Novedad_slide;

/**
 * Created by fcog on 8/10/14.
 */

public class SliderAdapter extends FragmentStatePagerAdapter {

    private int NUM_ITEMS;

    public SliderAdapter(FragmentManager fm, int num_slides) {
        super(fm);
        this.NUM_ITEMS = num_slides;
    }

    @Override
    public Fragment getItem(int position) {
        return Novedad_slide.newInstance(position);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }



/*    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }*/
}