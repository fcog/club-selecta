package com.elpais.app.clubselecta.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.elpais.app.clubselecta.FavoritosActivity;
import com.elpais.app.clubselecta.R;
import com.elpais.app.clubselecta.fragments.Favoritos;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.FavoritosDataSource;
import com.elpais.app.clubselecta.helpers.RequestManager;


/**
 * Created by fcog on 23/10/14.
 */

public class CategoriasAdapter extends BaseAdapter
{
    Context context;

    // 0: grid ; 1:list
    private int view_type;

    public DescuentosList items;

    boolean favoritos = false;

    LayoutInflater inflater;

    FavoritosDataSource datasource;


    public CategoriasAdapter(Context context, DescuentosList categoria_items, DescuentosList favoritos_ids, int view_type)
    {
        this.context=context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items=categoria_items;
        this.view_type=view_type;
        if (categoria_items == null) {
            this.items = favoritos_ids;
            this.favoritos = true;
        }
    }

    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;

        final int position_temp = position;

        if (convertView == null) {

            holder = new ViewHolder();

            if (view_type == 0) {
                convertView = inflater.inflate(R.layout.categorias_grid_item, null);
            }
            else{
                convertView = inflater.inflate(R.layout.categorias_list_item, null);
            }

            holder.slide_image = (NetworkImageView) convertView.findViewById(R.id.image_descuento_interna);
            holder.descuento_porcentaje = (TextView) convertView.findViewById(R.id.text_descuento_porcentaje);
            holder.descuento_titulo = (TextView) convertView.findViewById(R.id.text_descuento_titulo);
            holder.favorite_image = (ImageView) convertView.findViewById(R.id.image_descuento_favorito);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if ( !items.isEmpty() ) {

            if (favoritos){
                Log.i("Favorito Id", Integer.toString(items.get(position).getId()) );
            }
            else{
                Log.i("Descuento Id", Integer.toString(items.get(position).getId()) );
            }

            ImageLoader imageLoader = RequestManager.getInstance().getImageLoader();

            //slide_image.setDefaultImageResId(R.drawable.default_picture);
            holder.slide_image.setErrorImageResId(R.drawable.error);

            holder.slide_image.setImageUrl(items.get(position).getUrl_imagen(2), imageLoader);

            holder.slide_image.setScaleType(ImageView.ScaleType.FIT_XY);

            holder.descuento_porcentaje.setText(Integer.toString(items.get(position).getDescuento_silver())+"%");
            holder.descuento_titulo.setText(items.get(position).getEstablecimiento().getNombre());

            datasource = new FavoritosDataSource(context);
            datasource.open();

            if ( datasource.isFavorito( items.get(position).getId() ) ) {
                Log.i("Descuento Favorito Id", Integer.toString(items.get(position).getId()) );
                holder.favorite_image.setImageResource(R.drawable.ic_active_addfavoritos);
            }
            else{
                holder.favorite_image.setImageResource(R.drawable.ic_addfavoritos);
            }

            datasource.close();

            holder.favorite_image.setTag(items.get(position).getId());

            // click favorite icon
            holder.favorite_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int favorito_id = Integer.parseInt(view.getTag().toString());

                    ImageView favorite_image = (ImageView) view;

                    datasource.open();

                    // if favorite then remove from favorites
                    if ( datasource.isFavorito( favorito_id ) ) {
                        datasource.deleteFavorito(favorito_id);
                        favorite_image.setImageResource(R.drawable.ic_addfavoritos);
                        Log.i("CLICK", "borrar favorito");
                        if (favoritos){
//                            Intent intent = new Intent(context, FavoritosActivity.class);
//                            context.startActivity(intent);
                            items.remove(position_temp);
                            notifyDataSetChanged();
                        }
                    }
                    // if not favorite then add to favorites
                    else{
                        datasource.createFavorito(context, favorito_id);
                        favorite_image.setImageResource(R.drawable.ic_active_addfavoritos);
                        Log.i("CLICK", "crear favorito");
                    }

                    datasource.close();
                }
            });
        }

        return convertView;
    }

    private static class ViewHolder {
        NetworkImageView slide_image;
        TextView descuento_porcentaje;
        TextView descuento_titulo;
        ImageView favorite_image;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}

