package com.elpais.app.clubselecta;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.models.Descuento;
import com.elpais.app.clubselecta.models.Establecimiento;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class DescuentoActivity extends SherlockActivity {

    Descuento descuento;
    ArrayList<Establecimiento.Punto> puntos;

    MapView mapView;
    GoogleMap map;

    LatLng CALI = new LatLng(3.437006,-76.517415);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        int descuento_id = intent.getIntExtra("descuento_id", 0);

        if (savedInstanceState != null && ((RequestManager) getApplication()).getDescuentos() == null) {
            DescuentosList descuentos;
            descuentos = savedInstanceState.getParcelable("descuentos");
            ((RequestManager) getApplication()).setDescuentos(descuentos);
        }
        descuento = DescuentosList.getInstance().getDescuentoById(((RequestManager) getApplication()).getDescuentos(), descuento_id);

        Log.i("Descuento interna", Integer.toString(descuento_id));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_gradient_color));
        setTitle(descuento.getCategoria());

        setContentView(R.layout.activity_descuento);

        NetworkImageView slide_image = (NetworkImageView) findViewById(R.id.image_descuento_interna);

        ImageLoader imageLoader = RequestManager.getInstance().getImageLoader();

        //slide_image.setDefaultImageResId(R.drawable.default_picture);
        slide_image.setErrorImageResId(R.drawable.error);

        slide_image.setImageUrl(descuento.getUrl_imagen(1), imageLoader);

        TextView descuento_porcentaje = (TextView) findViewById(R.id.text_descuento_porcentaje);
        TextView descuento_titulo = (TextView) findViewById(R.id.text_descuento_titulo);
        TextView descuento_descripcion = (TextView) findViewById(R.id.text_descuento_descripcion);
        TextView descuento_body = (TextView) findViewById(R.id.text_descuento_body);
        LinearLayout layout_direccion = (LinearLayout) findViewById(R.id.layout_descuento_direccion);

        descuento_porcentaje.setText(Integer.toString(descuento.getDescuento_silver())+"%");
        descuento_titulo.setText(descuento.getTitulo());
        descuento_descripcion.setText(descuento.getDescripcion());

        if (descuento.getBody() == null || descuento.getBody().trim().equals("")){
            descuento_body.setVisibility(View.GONE);
        }
        else {
            descuento_body.setText((Html.fromHtml(descuento.getBody())));
        }

        puntos = descuento.getEstablecimiento().getPuntos();

        // load address information
        for (int i = 0; i < puntos.size(); i++) {
            // create a new textview
            final TextView descuento_direccion = new TextView(this);
            descuento_direccion.setTextAppearance(this, R.style.text_descuento_direccion);
            descuento_direccion.setGravity(Gravity.CENTER);

            // set some properties of rowTextView or something
            descuento_direccion.setText(puntos.get(i).getDireccion());

            // add the textview to the linearlayout
            layout_direccion.addView(descuento_direccion);
        }

        // load phone information
        if (descuento.getEstablecimiento().getTelefono() != 0) {

            LinearLayout layout_telefono = (LinearLayout) findViewById(R.id.layout_descuento_telefono);

            final TextView descuento_telefono_label = new TextView(this);
            descuento_telefono_label.setTextAppearance(this, R.style.text_descuento_direccion);
            descuento_telefono_label.setGravity(Gravity.CENTER);
            descuento_telefono_label.setText(R.string.telefono_label);

            layout_telefono.addView(descuento_telefono_label);

            final TextView descuento_telefono = new TextView(this);
            descuento_telefono.setTextAppearance(this, R.style.text_descuento_telefono);
            descuento_telefono.setGravity(Gravity.CENTER);
            descuento_telefono.setPadding(8,0,0,0);
            descuento_telefono.setText(Integer.toString(descuento.getEstablecimiento().getTelefono()));

            layout_telefono.addView(descuento_telefono);

            descuento_telefono.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:032"+descuento.getEstablecimiento().getTelefono()));
                    startActivity(callIntent);
                }
            });
        }

        //initialize map
        mapView = (MapView) findViewById(R.id.mapview_descuento);
        mapView.onCreate(savedInstanceState);

        if (!descuento.getEstablecimiento().getPuntos().isEmpty()) {

            map = mapView.getMap();

            if (map != null) {

                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.setMyLocationEnabled(true);

                // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                try {
                    MapsInitializer.initialize(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                LatLng ciudad;

                if (descuento.getEstablecimiento().getPuntos().get(0).getCiudad().equals("Cali")) {
                    ciudad = CALI;
                } else {
                    ciudad = new LatLng(
                            descuento.getEstablecimiento().getPuntos().get(0).getPunto().latitude,
                            descuento.getEstablecimiento().getPuntos().get(0).getPunto().longitude
                    );
                }

                CameraUpdate panToOrigin = CameraUpdateFactory.newLatLng(ciudad);
                map.moveCamera(panToOrigin);

                // set zoom level with animation
                map.animateCamera(CameraUpdateFactory.zoomTo(12), 400, null);

                int marker_icon_id;

                switch (descuento.getCategoria_id()) {
                    case 13:
                        marker_icon_id = R.drawable.ic_map_marker_deportivo;
                        break;
                    case 14:
                        marker_icon_id = R.drawable.ic_map_marker_entretenimiento;
                        break;
                    case 10:
                        marker_icon_id = R.drawable.ic_map_marker_saludybelleza;
                        break;
                    case 12:
                        marker_icon_id = R.drawable.ic_map_marker_restaurantes;
                        break;
                    case 17:
                        marker_icon_id = R.drawable.ic_map_marker_otros;
                        break;
                    case 11:
                        marker_icon_id = R.drawable.ic_map_marker_automoviles;
                        break;
                    case 15:
                        marker_icon_id = R.drawable.ic_map_marker_calzadoyropa;
                        break;
                    default:
                        marker_icon_id = R.drawable.marker_todos;
                }

                for (Establecimiento.Punto punto : puntos) {
                    map.addMarker(new MarkerOptions()
                                    .position(punto.getPunto())
                                    .title(descuento.getEstablecimiento().getNombre())
                                    .snippet(punto.getDireccion())
                                    .icon(BitmapDescriptorFactory.fromResource(marker_icon_id))
                    );
                }
            }
        }
        // if no points, no map
        else{
            mapView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable("descuentos", ((RequestManager) getApplication()).getDescuentos());
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu_descuento, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_favoritos:
                Intent intent1 = new Intent(getApplicationContext(), FavoritosActivity.class);
                startActivity(intent1);
                return true;
            case R.id.action_alerta:
                Intent intent2 = new Intent(getApplicationContext(), AlertasActivity.class);
                startActivity(intent2);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
