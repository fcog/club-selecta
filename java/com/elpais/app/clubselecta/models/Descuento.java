package com.elpais.app.clubselecta.models;

import com.elpais.app.clubselecta.helpers.AppConst;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fcog on 10/10/14.
 */
public class Descuento {
    private int id;
    private Date updated;
    private String imagen;
    private String titulo;
    private String body;
    private String descripcion;
    private int descuento_silver;
//    private int descuento_black;
    private String categoria;
    private int categoria_id;
    private Date fecha_inicial;
    private Date fecha_final;
    private Boolean novedad;
    private Establecimiento establecimiento;

    public Descuento(){
    }

    public Descuento(int id,
                     Establecimiento establecimiento,
                     int updated,
                     String imagen,
                     String titulo,
                     String body,
                     String descripcion,
                     int descuento_silver,
                     String categoria,
                     int categoria_id,
                     String fecha_inicial,
                     String fecha_final,
                     Boolean novedad) {

        SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        this.id = id;
        this.establecimiento = establecimiento;
        this.updated = new Date( updated * 1000 );
        this.imagen = imagen;
        this.titulo = titulo;
        this.body = body;
        this.descripcion = descripcion;
        this.descuento_silver = descuento_silver;
        this.categoria = categoria;
        this.categoria_id = categoria_id;
        this.novedad = novedad;

        try {
            this.fecha_inicial = dateParser.parse(fecha_inicial);
            this.fecha_final = dateParser.parse(fecha_final);
        }
        catch (ParseException exception){
            this.fecha_inicial = null;
            this.fecha_final = null;
        }
    }

    public int getId() {
        return id;
    }

    public Date getUpdated() {
        return updated;
    }

    public String getImagen() {
        return imagen;
    }

    public Boolean getNovedad() {
        return novedad;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getBody() {
        return body;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getDescuento_silver() {
        return descuento_silver;
    }

    public Date getFecha_inicial() {
        return fecha_inicial;
    }

    public Date getFecha_final() {
        return fecha_final;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public String getCategoria() {
        return categoria;
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public String getUrl_imagen(int tipo) {
        String directory;
        switch (tipo){
            case 1:
                //imagen para la interior de descuento
                directory = AppConst.directory_descuento_interna;
                break;
            case 2:
                //imagen para la seccion por categorias
                directory = AppConst.directory_descuento_categoria;
                break;
            default:
                //imagen para la seccion novedad
                directory = AppConst.directory_descuento_novedad;
                break;
        }

        return directory + this.imagen;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setDescuento_silver(int descuento_silver) {
        this.descuento_silver = descuento_silver;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public void setFecha_inicial(Date fecha_inicial) {
        this.fecha_inicial = fecha_inicial;
    }

    public void setFecha_final(Date fecha_final) {
        this.fecha_final = fecha_final;
    }

    public void setNovedad(Boolean novedad) {
        this.novedad = novedad;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }
}
