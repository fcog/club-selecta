package com.elpais.app.clubselecta.models;


import android.util.Log;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by fcog on 10/10/14.
 */
public class Establecimiento {

    public int id;
    public String nombre;
    public int telefono;
    public ArrayList<Punto> puntos = new ArrayList<Punto>();

    public Establecimiento() {
    }

    public Establecimiento(int id, String nombre, int telefono) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public void AddPunto(String lat, String lng, String direccion, String ciudad) {
        Punto punto = new Punto(lat, lng, direccion, ciudad);
        try {
            this.puntos.add(punto);
        }
        catch (Exception e){
            Log.e("error", e.toString());
        }
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public ArrayList<Punto> getPuntos() {
        return puntos;
    }

    static public class Punto {

        public final String direccion;
        public final String ciudad;
        public final LatLng punto;

        public Punto(String lat, String lng, String direccion, String ciudad) {

            this.punto = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
            this.direccion = direccion;
            this.ciudad = ciudad;
        }

        public LatLng getPunto() {
            return punto;
        }

        public String getDireccion() {
            return direccion;
        }

        public String getCiudad() {
            return ciudad;
        }
    }

    public void setPuntos(ArrayList<Punto> puntos) {
        this.puntos = puntos;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
