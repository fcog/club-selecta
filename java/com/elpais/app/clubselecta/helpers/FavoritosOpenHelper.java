package com.elpais.app.clubselecta.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by fcog on 26/11/14.
 */

public class FavoritosOpenHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "favoritos.db";
    public static final String TABLE_NAME = "favoritos";
    public static final String COLUMN_ID = "id";
    public static final String DESCUENTO_ID = "descuento_id";
    public static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    DESCUENTO_ID + " integer not null);";
    public static final String TABLE_UPDATE = "create unique index unique_name on "+TABLE_NAME+"("+DESCUENTO_ID+");";

    FavoritosOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_UPDATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_UPDATE);
    }
}
