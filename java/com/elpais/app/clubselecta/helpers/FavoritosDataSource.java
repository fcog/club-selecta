package com.elpais.app.clubselecta.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by fcog on 26/11/14.
 */
public class FavoritosDataSource {

    // Database fields
    private SQLiteDatabase database;
    private FavoritosOpenHelper dbHelper;
    private String[] allColumns = { FavoritosOpenHelper.DESCUENTO_ID };

    public FavoritosDataSource(Context context) {
        dbHelper = new FavoritosOpenHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createFavorito(Context context, int descuento_id) {
        ContentValues values = new ContentValues();
        values.put(FavoritosOpenHelper.DESCUENTO_ID, descuento_id);
        try {
            database.insertOrThrow(FavoritosOpenHelper.TABLE_NAME, null, values);
        }
        catch (SQLException e){
            Toast.makeText(context, "Este descuento ya está en tus favoritos", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteFavorito(int descuento_id) {
        database.delete(FavoritosOpenHelper.TABLE_NAME, FavoritosOpenHelper.DESCUENTO_ID
                + " = " + descuento_id, null);
    }

    public ArrayList<Integer> getFavoritos() {
        ArrayList<Integer> favoritos = new ArrayList<Integer>();

        Cursor cursor = database.query(FavoritosOpenHelper.TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int descuento_id = cursor.getInt(0);
            favoritos.add(descuento_id);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return favoritos;
    }


    public boolean isFavorito(int descuento_id){
        String queryf = "select * from " + FavoritosOpenHelper.TABLE_NAME + " where " + FavoritosOpenHelper.DESCUENTO_ID  + "='" + descuento_id + "'";

        Cursor c = database.rawQuery(queryf, null);
        if (c.getCount() == 0) {
            c.close();
            return false;
        }else {
            c.close();
            return true;
        }
    }

}
