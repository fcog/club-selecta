package com.elpais.app.clubselecta.helpers;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.elpais.app.clubselecta.models.Descuento;
import com.elpais.app.clubselecta.models.Establecimiento;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by fcog on 18/11/14.
 */
public class DescuentosList extends ArrayList<Descuento> implements Parcelable {

    private static DescuentosList mInstance = null;

    public DescuentosList(){

    }

    //singleton pattern
    public static synchronized DescuentosList getInstance(){
        if(null == mInstance){
            mInstance = new DescuentosList();
        }
        return mInstance;
    }

    public Descuento getDescuentoById(DescuentosList descuentos, int descuento_id){

        for (Descuento descuento: descuentos){
            if (descuento.getId() == descuento_id){
                return descuento;
            }
        }

        return new Descuento();
    }

    public DescuentosList getNovedadesItems(DescuentosList descuentos){
        DescuentosList novedad_items = new DescuentosList();

        for (Descuento descuento: descuentos){
            if (descuento.getNovedad()){
                novedad_items.add(descuento);
            }
        }

        return novedad_items;
    }

    public DescuentosList getCategoriaItems(DescuentosList descuentos, int categoria_id){
        DescuentosList categoria_items = new DescuentosList();

        if (categoria_id == 0){
            for (Descuento descuento: descuentos){
                categoria_items.add(descuento);
            }
        }
        else{
            for (Descuento descuento: descuentos){
                if (descuento.getCategoria_id() == categoria_id){
                    categoria_items.add(descuento);
                }
            }
        }

        return categoria_items;
    }

    public DescuentosList loadFavoritos(DescuentosList descuentos, ArrayList<Integer> descuentos_ids){

        // loop algorithm to search Favoritos from the Sqlite database in the internet memory variable ("synchronization")
        int encontrados = 0;

        for (Descuento descuento: descuentos){
            for (int descuento_id: descuentos_ids) {
                if (descuento.getId() == descuento_id) {
                    this.add(descuento);
                    encontrados++;
                    break;
                }
            }
            if (encontrados == descuentos_ids.size()){
                break;
            }
        }

        return this;
    }


    public DescuentosList(Parcel in){
        readFromParcel(in);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public DescuentosList createFromParcel(Parcel in) {
            return new DescuentosList(in);
        }

        public Object[] newArray(int arg0) {
            return null;
        }
    };

    private void readFromParcel(Parcel in) {

        this.clear();

        //First we have to read the list size
        int size = in.readInt();

        //Reading remember that we wrote first the Name and later the Phone Number.
        //Order is fundamental
        for (int i = 0; i < size; i++) {
            Descuento d = new Descuento();
            Establecimiento e = new Establecimiento();
            ArrayList<Establecimiento.Punto> puntos = new ArrayList<Establecimiento.Punto>();

            int size2 = in.readInt();

            for (int j = 0; j < size2; j++) {
                Establecimiento.Punto p = new Establecimiento.Punto(
                        in.readString(),
                        in.readString(),
                        in.readString(),
                        in.readString()
                );

                puntos.add(p);
            }

            e.setPuntos(puntos);

            e.setId(in.readInt());
            e.setNombre(in.readString());
            e.setTelefono(in.readInt());
//            in.readTypedList(puntos, DescuentosList);
//            e.setPuntos();

            d.setId(in.readInt());
            d.setUpdated(new Date( in.readLong() ));
            d.setImagen(in.readString());
            d.setTitulo(in.readString());
            d.setBody(in.readString());
            d.setDescripcion(in.readString());
            d.setDescuento_silver(in.readInt());
            d.setCategoria(in.readString());
            d.setNovedad(Boolean.parseBoolean(in.readString()));
            d.setFecha_final(new Date( in.readLong() ));
            d.setFecha_inicial(new Date( in.readLong() ));
            d.setEstablecimiento(e);
            this.add(d);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        int size = this.size();


        //We have to write the list size, we need him recreating the list
        parcel.writeInt(size);

        //We decided arbitrarily to write first the Name and later the Phone Number.
        for (int i = 0; i < size; i++) {

            Descuento d = this.get(i);
            Establecimiento e = d.getEstablecimiento();

            int size2 = e.getPuntos().size();

            parcel.writeInt(size2);

            for (int j = 0; j < size2; j++) {
                parcel.writeString(Double.toString(e.getPuntos().get(j).getPunto().latitude));
                parcel.writeString(Double.toString(e.getPuntos().get(j).getPunto().longitude));
                parcel.writeString(e.getPuntos().get(j).getDireccion());
                parcel.writeString(e.getPuntos().get(j).getCiudad());
            }

            parcel.writeInt(e.getId());
            parcel.writeString(e.getNombre());
            parcel.writeInt(e.getTelefono());
            parcel.writeInt(d.getId());
            parcel.writeLong( d.getUpdated().getTime() );
            parcel.writeString(d.getImagen());
            parcel.writeString(d.getTitulo());
            parcel.writeString(d.getBody());
            parcel.writeString(d.getDescripcion());
            parcel.writeInt(d.getDescuento_silver());
            parcel.writeString(d.getCategoria());
            parcel.writeString(Boolean.toString(d.getNovedad()));
            parcel.writeLong(d.getFecha_final().getTime());
            parcel.writeLong(d.getFecha_inicial().getTime());

        }
    }
}
