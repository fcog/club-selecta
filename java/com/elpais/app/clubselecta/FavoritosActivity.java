package com.elpais.app.clubselecta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.elpais.app.clubselecta.adapters.CategoriasAdapter;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.FavoritosDataSource;
import com.elpais.app.clubselecta.helpers.RequestManager;

import java.util.ArrayList;


public class FavoritosActivity extends SherlockFragmentActivity {

    public CategoriasAdapter CategoriasAdapter;

    public DescuentosList favoritos_items = new DescuentosList();

    GridView gridview;
    ListView listview;

    RelativeLayout category_body;

    // 0: grid ; 1:list
    int actual_view = 0;

    ImageView view_icon;

    SharedPreferences prefs;


    private static final String TAG_FAVORITE_PREF = "actual_view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_gradient_color));

        if ( !(((RequestManager) getApplication()).getDescuentos() == null) ) {

            gridview = (GridView) findViewById(R.id.descuentos_gridview);
            listview = (ListView) findViewById(R.id.descuentos_listview);
            category_body = (RelativeLayout) findViewById(R.id.categoria_body);

            view_icon = (ImageView) findViewById(R.id.view_icon);

            prefs = this.getSharedPreferences("com.elpais.app.clubselecta", MODE_PRIVATE);

            //if user has chosen a view before
            actual_view = prefs.getInt(TAG_FAVORITE_PREF, 0);

            FavoritosDataSource datasource = new FavoritosDataSource(this);

            datasource.open();

            ArrayList<Integer> favoritos_ids = datasource.getFavoritos();

            datasource.close();

            favoritos_items.loadFavoritos(((RequestManager) getApplication()).getDescuentos(), favoritos_ids);

            load_adapter(favoritos_items, actual_view);

            view_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (actual_view == 0) {
                        load_adapter(favoritos_items, 1);
                    } else {
                        load_adapter(favoritos_items, 0);
                    }
                }
            });

            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getApplicationContext(), DescuentoActivity.class);
                    intent.putExtra("descuento_id", favoritos_items.get(i).getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getApplicationContext(), DescuentoActivity.class);
                    intent.putExtra("descuento_id", favoritos_items.get(i).getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        }
    }


    public void load_adapter(final DescuentosList favoritos_items, int actual){

        int icon_id;

        if (actual == 0) {
            CategoriasAdapter = new CategoriasAdapter(this, null, favoritos_items, 0);
            gridview.setAdapter(CategoriasAdapter);

            icon_id = R.drawable.ic_active_list;
            actual_view = 0;

            //add margin to grid only
            category_body.setPadding(20,20,20,0);

            gridview.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }
        else {
            CategoriasAdapter = new CategoriasAdapter(this, null, favoritos_items, 1);
            listview.setAdapter(CategoriasAdapter);

            icon_id = R.drawable.ic_grid;
            actual_view = 1;

            category_body.setPadding(0,0,0,0);

            gridview.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
        }

        Log.i(TAG_FAVORITE_PREF, Integer.toString(actual_view));

        // change icon
        view_icon.setImageResource(icon_id);

        //persist user's view choice
        prefs.edit().putInt(TAG_FAVORITE_PREF, actual_view).apply();
    }


    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu_favoritos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
