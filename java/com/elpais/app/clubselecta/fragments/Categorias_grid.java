package com.elpais.app.clubselecta.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.elpais.app.clubselecta.R;


/**
 * Created by fcog on 23/10/14.
 */
public class Categorias_grid extends SherlockFragment {

    public Categorias_grid() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.categorias_grid, container, false);

        return view;
    }


}
