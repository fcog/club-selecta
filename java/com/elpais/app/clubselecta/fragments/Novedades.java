package com.elpais.app.clubselecta.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.elpais.app.clubselecta.R;
import com.elpais.app.clubselecta.adapters.SliderAdapter;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.helpers.SimpleViewPagerIndicator;


public class Novedades extends SherlockFragment {

    ViewPager mPager;
    PagerAdapter mPagerAdapter = null;
    SimpleViewPagerIndicator pageIndicator;

    int num_slides;

    public Novedades() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.i("Novedad", "On create");

        return inflater.inflate(R.layout.fragment_novedades, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) view.findViewById(R.id.pager);

        //Inicializa paginador
        pageIndicator = (SimpleViewPagerIndicator) view.findViewById(R.id.page_indicator);

        num_slides = DescuentosList.getInstance().getNovedadesItems( ((RequestManager) getActivity().getApplication()).getDescuentos() ).size();

        mPagerAdapter = new SliderAdapter(getChildFragmentManager(), num_slides);
        mPager.setAdapter(mPagerAdapter);

        pageIndicator.setViewPager(mPager);
        pageIndicator.notifyDataSetChanged();
    }
}
