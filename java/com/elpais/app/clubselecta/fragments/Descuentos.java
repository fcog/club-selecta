package com.elpais.app.clubselecta.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.elpais.app.clubselecta.R;

/**
 * Created by fcog on 10/11/14.
 */
public class Descuentos extends SherlockFragment {


    public Descuentos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_descuentos, container, false);
    }


}
