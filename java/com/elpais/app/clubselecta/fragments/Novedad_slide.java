package com.elpais.app.clubselecta.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.elpais.app.clubselecta.DescuentoActivity;
import com.elpais.app.clubselecta.MainActivity;
import com.elpais.app.clubselecta.R;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.models.Descuento;


public class Novedad_slide extends SherlockFragment  {

    public static int slides = 0;

    public Descuento descuento;

    public static Novedad_slide newInstance(int position) {
        Novedad_slide f = new Novedad_slide();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        slides++;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_novedad_slide, container, false);

        int position = this.getArguments().getInt("position");

        int descuento_id = DescuentosList.getInstance().getNovedadesItems( ((RequestManager) getActivity().getApplication()).getDescuentos() ).get(position).getId();

        if (((RequestManager) getActivity().getApplication()).getDescuentos() != null) {

            Log.i("descuento id dado", Integer.toString(descuento_id));

            descuento = DescuentosList.getInstance().getDescuentoById(((RequestManager) getActivity().getApplication()).getDescuentos(), descuento_id);

            Log.i("descuento id obtenido", Integer.toString(descuento.getId()));

            String slide_image_url = descuento.getUrl_imagen(0);

            ImageLoader slide_image_loader = RequestManager.getInstance().getImageLoader();

            NetworkImageView slide_image = (NetworkImageView) view.findViewById(R.id.slide);

            //slide_image.setDefaultImageResId(R.drawable.default_picture);
            slide_image.setErrorImageResId(R.drawable.error);

            slide_image.setImageUrl(slide_image_url, slide_image_loader);

            slide_image.setScaleType(ImageView.ScaleType.FIT_XY);

//            slide_image.setOnLongClickListener(new View.OnLongClickListener() {

//                @Override
//                public boolean onLongClick(View view) {
//                      Intent intent = new Intent(getActivity(), DescuentoActivity.class);
//                      intent.putExtra("position", position);
//                      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                      startActivity(intent);
//                    return false;
//                }
//            });

            slide_image.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
//                    Intent intent = new Intent();
//                    intent.setAction(Intent.ACTION_VIEW);
//                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
//                    intent.setData(Uri.parse("http://google.com"));
//                    startActivity(intent);
                    Intent intent = new Intent(getActivity(), DescuentoActivity.class);
                    intent.putExtra("descuento_id", descuento.getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        }

        return view;
    }

}
