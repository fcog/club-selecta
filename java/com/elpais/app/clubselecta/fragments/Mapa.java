package com.elpais.app.clubselecta.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.internal.widget.IcsAdapterView;
import com.actionbarsherlock.internal.widget.IcsSpinner;
import com.elpais.app.clubselecta.R;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.GPSTracker;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.models.Descuento;
import com.elpais.app.clubselecta.models.Establecimiento;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Arrays;
import java.util.List;


public class Mapa extends SherlockFragment {

    public static DescuentosList categoria_items;
    int categoria_escogida_spinner = 0;
    int categoria_id = 0;
    ArrayAdapter<CharSequence> adapter;

    MapView mapView;
    GoogleMap map;

    GPSTracker gps;

    public Mapa() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_mapa, container, false);

        mapView = (MapView) rootView.findViewById(R.id.mapview);

        MapsInitializer.initialize(getActivity());

        final List<String> categorias_id = Arrays.asList((getResources().getStringArray(R.array.categorias_id2)));
        final List<String> categorias = Arrays.asList((getResources().getStringArray(R.array.categorias2)));
        String categoria = categorias.get(categoria_escogida_spinner);

        if (savedInstanceState != null){
            categoria_escogida_spinner = savedInstanceState.getInt("categoria_escogida_spinner", 0);
        }

        categoria_id = Integer.parseInt(categorias_id.get(categoria_escogida_spinner));

        // set spinner information
        IcsSpinner spinner = (IcsSpinner) rootView.findViewById(R.id.mapa_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapter = ArrayAdapter.createFromResource(getActivity(), R.array.categorias2, R.layout.categoria_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.categoria_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        // set spinner position
        int spinnerPosition = adapter.getPosition(categoria);
        spinner.setSelection(spinnerPosition);

        spinner.setOnItemSelectedListener(new IcsAdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(IcsAdapterView<?> icsAdapterView, View view, int i, long l) {

                // show all descuentos
                if (i == 0){
                    categoria_id = 0;
                }
                // show descuento by category chosen
                else{
                    categoria_id = Integer.parseInt(categorias_id.get(i));
                }

                getMarcadores(categoria_id);
                categoria_escogida_spinner = i;
            }

            @Override
            public void onNothingSelected(IcsAdapterView<?> icsAdapterView) {

            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapView.onCreate(savedInstanceState);

        map = mapView.getMap();

        if (map != null) {

            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);

            // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
            try {
                MapsInitializer.initialize(this.getActivity());
            } catch (Exception e) {
                e.printStackTrace();
            }

            gps = new GPSTracker(getActivity());

            // check if GPS enabled
            if(gps.canGetLocation()){

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(latitude, longitude), 13));

                // Sets the center of the map to location user
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude))
                        .zoom(14)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                getMarcadores(categoria_id);

            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    public void getMarcadores(int categoria_id){

        map.clear();

        int marker_icon_id;

        categoria_items = DescuentosList.getInstance().getCategoriaItems(((RequestManager) getActivity().getApplication()).getDescuentos(), categoria_id);

        for (Descuento descuento: categoria_items) {

            switch (descuento.getCategoria_id()) {
                case 13:
                    marker_icon_id = R.drawable.ic_map_marker_deportivo;
                    break;
                case 14:
                    marker_icon_id = R.drawable.ic_map_marker_entretenimiento;
                    break;
                case 10:
                    marker_icon_id = R.drawable.ic_map_marker_saludybelleza;
                    break;
                case 12:
                    marker_icon_id = R.drawable.ic_map_marker_restaurantes;
                    break;
                case 17:
                    marker_icon_id = R.drawable.ic_map_marker_otros;
                    break;
                case 11:
                    marker_icon_id = R.drawable.ic_map_marker_automoviles;
                    break;
                case 15:
                    marker_icon_id = R.drawable.ic_map_marker_calzadoyropa;
                    break;
                default:
                    marker_icon_id = R.drawable.marker_todos;
            }

            for (Establecimiento.Punto punto : descuento.getEstablecimiento().getPuntos()) {
                map.addMarker(new MarkerOptions()
                                .position(punto.getPunto())
                                .title(descuento.getEstablecimiento().getNombre())
                                .snippet(punto.getDireccion())
                                .icon(BitmapDescriptorFactory.fromResource(marker_icon_id))
                );
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("categoria_escogida_spinner", categoria_escogida_spinner);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
