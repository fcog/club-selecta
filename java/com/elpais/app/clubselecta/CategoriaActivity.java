package com.elpais.app.clubselecta;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.internal.widget.IcsAdapterView;
import com.actionbarsherlock.internal.widget.IcsSpinner;
import com.elpais.app.clubselecta.adapters.CategoriasAdapter;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.RequestManager;

import java.util.Arrays;
import java.util.List;


public class CategoriaActivity extends SherlockFragmentActivity {

    public CategoriasAdapter CategoriasAdapter;

    public static DescuentosList categoria_items;

    GridView gridview;
    ListView listview;

    IcsSpinner spinner;

    RelativeLayout category_body;

    // 0: grid ; 1:list
    int actual_view = 0;

    //category chosen on main grid, default All categories
    String categoria_pos = "7";

    ImageView view_icon;

    SharedPreferences prefs;

    private static final String TAG_PREF = "actual_view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // when app memory is destroyed then reload it from Bundle
        if (savedInstanceState != null && ((RequestManager) getApplication()).getDescuentos() == null) {
            DescuentosList descuentos;
            descuentos = savedInstanceState.getParcelable("descuentos");
            ((RequestManager) getApplication()).setDescuentos(descuentos);

            //getString works from API level 12
            categoria_pos = getIntent().hasExtra("categoria_pos") ? savedInstanceState.getString("categoria_pos", "7") : "7";
        }

        // get category id from main activity
        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            categoria_pos = intent.getStringExtra("categoria_pos");
        }

        //get category name and id from fragment_descuentos.xml layout grid position
        final List<String> categorias_id = Arrays.asList((getResources().getStringArray(R.array.categorias_id)));
        final List<String> categorias = Arrays.asList((getResources().getStringArray(R.array.categorias)));
        String categoria = categorias.get(Integer.parseInt(categoria_pos));
        int categoria_id = Integer.parseInt(categorias_id.get(Integer.parseInt(categoria_pos)));

        Log.i("Spinner index", categoria_pos);

        setContentView(R.layout.activity_categoria);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_gradient_color));

        // set spinner information
        spinner = (IcsSpinner) findViewById(R.id.categorias_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.categorias, R.layout.categoria_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.categoria_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        // set spinner position
        int spinnerPosition = adapter.getPosition(categoria);
        spinner.setSelection(spinnerPosition);

        spinner.setOnItemSelectedListener(new IcsAdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(IcsAdapterView<?> icsAdapterView, View view, int i, long l) {

                // show all descuentos
                if (i == 7){
                    categoria_items = ((RequestManager) getApplication()).getDescuentos();
                }
                // show descuento by category chosen
                else{
                    int categoria_id = Integer.parseInt(categorias_id.get(i));

                    categoria_items = DescuentosList.getInstance().getCategoriaItems(((RequestManager) getApplication()).getDescuentos(), categoria_id);
                }

                load_adapter(categoria_items, actual_view);
            }

            @Override
            public void onNothingSelected(IcsAdapterView<?> icsAdapterView) {

            }
        });

        gridview = (GridView) findViewById(R.id.descuentos_gridview);
        listview = (ListView) findViewById(R.id.descuentos_listview);
        category_body = (RelativeLayout) findViewById(R.id.categoria_body);

        categoria_items = DescuentosList.getInstance().getCategoriaItems(((RequestManager) getApplication()).getDescuentos(), categoria_id);

        Log.i("Descuento No items", Integer.toString( categoria_items.size() ));

        view_icon = (ImageView) findViewById(R.id.view_icon);

        prefs = this.getSharedPreferences("com.elpais.app.clubselecta", MODE_PRIVATE);

        //if user has chosen a view before
        actual_view = prefs.getInt(TAG_PREF, 0);

        load_adapter(categoria_items, actual_view);

        view_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actual_view == 0) {
                    load_adapter(categoria_items, 1);
                }
                else{
                    load_adapter(categoria_items, 0);
                }
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), DescuentoActivity.class);
                intent.putExtra("descuento_id", categoria_items.get(i).getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), DescuentoActivity.class);
                intent.putExtra("descuento_id", categoria_items.get(i).getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }


    public void load_adapter(final DescuentosList categoria_items, int actual){

        int icon_id;

        if (actual == 0) {
            CategoriasAdapter = new CategoriasAdapter(this, categoria_items, null, 0);
            gridview.setAdapter(CategoriasAdapter);

            icon_id = R.drawable.ic_active_list;
            actual_view = 0;

            //add margin to grid only
            category_body.setPadding(20,20,20,0);

            gridview.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }
        else {
            CategoriasAdapter = new CategoriasAdapter(this, categoria_items, null, 1);
            listview.setAdapter(CategoriasAdapter);

            icon_id = R.drawable.ic_grid;
            actual_view = 1;

            category_body.setPadding(0,0,0,0);

            gridview.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
        }

        Log.i(TAG_PREF, Integer.toString(actual_view));

        // change icon
        ImageView imageView = (ImageView) view_icon;
        imageView.setImageResource(icon_id);

        //persist user's view choice
        prefs.edit().putInt(TAG_PREF, actual_view).apply();
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu_categoria, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_favoritos:
                Intent intent1 = new Intent(getApplicationContext(), FavoritosActivity.class);
                startActivity(intent1);
                return true;
            case R.id.action_alerta:
                Intent intent2 = new Intent(getApplicationContext(), AlertasActivity.class);
                startActivity(intent2);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable("descuentos", ((RequestManager) getApplication()).getDescuentos());
        outState.putString("categoria_pos", categoria_pos);
    }

}
