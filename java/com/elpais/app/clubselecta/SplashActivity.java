package com.elpais.app.clubselecta;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.elpais.app.clubselecta.helpers.AppConst;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.FavoritosDataSource;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.models.Descuento;
import com.elpais.app.clubselecta.models.Establecimiento;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SplashActivity extends SherlockFragmentActivity {

    public static final String tag_json_array = "json_array_req";
    public static final String log_json_error = "JSON PARSE ERROR";
    public static final String log_volley_error = "VOLLEY ERROR";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);    // Removes title bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);    // Removes notification bar

        setContentView(R.layout.activity_splash);

        // get api info
        JsonArrayRequest req = new JsonArrayRequest(AppConst.API_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        ((RequestManager) getApplication()).setDescuentos(parser(response));

                        // String the main activity
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);

                        // closing splash activity
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(log_volley_error, "Volley error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), R.string.error_cargando, Toast.LENGTH_LONG).show();

                // String the main activity
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

                // closing splash activity
                finish();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(5000,2,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestManager.getInstance().addToRequestQueue(req, tag_json_array);
    }

    private DescuentosList parser(JSONArray response){

        DescuentosList descuentosAux = new DescuentosList();

        for(int i = 0; i<response.length(); i++){

            try {
                JSONObject jsonObject = (JSONObject) response.get(i);

                int id = jsonObject.getInt("id");
                int updated = jsonObject.getInt("updated");
                String imagen = jsonObject.getString("imagen");
                String titulo = jsonObject.getString("titulo");
                String body = jsonObject.getString("body");
                String descripcion = jsonObject.getString("descripcion");
                Boolean novedad = jsonObject.getBoolean("novedad");
                int descuento_silver = jsonObject.getInt("descuento");
                String categoria = jsonObject.getString("categoria");
                int categoria_id = jsonObject.getInt("categoria_id");
                int telefono = jsonObject.getInt("telefono");
                int establecimiento_id = jsonObject.getInt("establecimiento_id");
                String fecha_inicial = jsonObject.getString("fecha_inicial");
                String fecha_final = jsonObject.getString("fecha_final");

                JSONArray puntosAux = jsonObject.getJSONArray("puntos");

                Establecimiento establecimiento = new Establecimiento(establecimiento_id, titulo, telefono);

                for(int j = 0; j<puntosAux.length(); j++) {
                    JSONObject jsonObject2 = (JSONObject) puntosAux.get(j);

                    String lat = jsonObject2.getString("lat");
                    String lng = jsonObject2.getString("lng");
                    String direccion = jsonObject2.getString("direccion");
                    String ciudad = jsonObject2.getString("ciudad");

                    establecimiento.AddPunto(lat, lng, direccion, ciudad);
                }

                Descuento descuento = new Descuento(
                        id,
                        establecimiento,
                        updated,
                        imagen,
                        titulo,
                        body,
                        descripcion,
                        descuento_silver,
                        categoria,
                        categoria_id,
                        fecha_inicial,
                        fecha_final,
                        novedad
                );

                //Log.i("dato", descuento.getUrl_imagen(0));

                descuentosAux.add(descuento);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(log_json_error, e.toString());
            }
        }

        return descuentosAux;
    }
}
