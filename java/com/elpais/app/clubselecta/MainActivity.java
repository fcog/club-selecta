package com.elpais.app.clubselecta;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.elpais.app.clubselecta.fragments.Descuentos;
import com.elpais.app.clubselecta.fragments.Mapa;
import com.elpais.app.clubselecta.fragments.Novedades;
import com.elpais.app.clubselecta.helpers.DescuentosList;
import com.elpais.app.clubselecta.helpers.RequestManager;
import com.elpais.app.clubselecta.helpers.TabListener;


public class MainActivity extends SherlockFragmentActivity {

    private static final String TAG = "Activity Cycle";
    private static final String TAG2 = "Bundle";

    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate enter");
        super.onCreate(savedInstanceState);

        //Bundle bundle = getIntent().getExtras();

        if (savedInstanceState != null && ((RequestManager) getApplication()).getDescuentos() == null) {
            DescuentosList descuentos;
            descuentos = savedInstanceState.getParcelable("descuentos");
            ((RequestManager) getApplication()).setDescuentos(descuentos);

            Log.i(TAG2, Integer.toString( ((RequestManager) getApplication()).getDescuentos().size() ));
        }

//        Log.i("Descuentos", Integer.toString(descuentos.size()));

        setContentView(R.layout.main_activity);

        ActionBar actionBar = getSupportActionBar();
        // Hide Actionbar Title
        actionBar.setDisplayShowTitleEnabled(false);

        actionBar.setIcon(R.drawable.logo_club_selecta);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_gradient_color));

        //if data was fetch from API load tabs with fragments
        if ( !(((RequestManager) getApplication()).getDescuentos() == null) ) {

            Fragment novedades_fragment = new Novedades();
            Fragment mapa_fragment = new Mapa();
            Fragment descuentos_fragment = new Descuentos();

            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            actionBar.addTab(actionBar.newTab().setText(R.string.menu_tab1).setTabListener(new TabListener(novedades_fragment)));
            actionBar.addTab(actionBar.newTab().setText(R.string.menu_tab2).setTabListener(new TabListener(mapa_fragment)));
            actionBar.addTab(actionBar.newTab().setText(R.string.menu_tab3).setTabListener(new TabListener(descuentos_fragment)));
        }

        Log.i(TAG, "onCreate exit");
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_favoritos:
                Intent intent1 = new Intent(getApplicationContext(), FavoritosActivity.class);
                startActivity(intent1);
                return true;
            case R.id.action_alerta:
                Intent intent2 = new Intent(getApplicationContext(), AlertasActivity.class);
                startActivity(intent2);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openCategoria(View view){
        Intent intent = new Intent(this, CategoriaActivity.class);
        intent.putExtra("categoria_pos", view.getTag().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onDestroy()
    {
        Log.i(TAG, "onDestroy enter");
        super.onDestroy();
        Log.i(TAG, "onDestroy exit");
    }
    @Override
    protected void onPause()
    {
        Log.i(TAG, "onPause enter");
        super.onPause();
        Log.i(TAG, "onPause exit");
    }
    @Override
    protected void onResume()
    {
        Log.i(TAG, "onResume enter");
        super.onResume();
        Log.i(TAG, "onResume exit");
    }
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        Log.i(TAG, "onSaveInstanceState enter");
        super.onSaveInstanceState(outState);
        outState.putSerializable("descuentos", ((RequestManager) getApplication()).getDescuentos());
        Log.i(TAG, "onSaveInstanceState exit");
    }
    @Override
    protected void onStart()
    {
        Log.i(TAG, "onStart enter");
        super.onStart();
        Log.i(TAG, "onStart exit");
    }
    @Override
    protected void onStop()
    {
        Log.i(TAG, "onStop enter");
        super.onStop();
        Log.i(TAG, "onStop exit");
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        Log.i(TAG, "onRestoreInstanceState enter");
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState exit");
    }
}
